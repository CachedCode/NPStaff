/* Staff Listing Plugin for the NovaPixel Server Network

    Will hook into the PermissionsEx plugin API via the pom.xml file via the use of Maven. The main priority of the specifically developed plugin will be to list
    online ranks and permissions of players on any server across the network. A member of the network with administrative privileges or with access to the network's
    FTP panel may be able to configure the plugin's listed ranks and cater them to their own. A member may select the ranks that are to be displayed when a player uses 
    the command /staff, as well as the names of said ranks, colours and style. The list will be updated with every time /staff is executed, if a member is removed from a
    particular rank it will update the /staff list with the new data. Plugin will be made to work efficiently and without error.
    
    Developed by InfiniteLoophole -- Developer of the NovaPixel Server Network

*/