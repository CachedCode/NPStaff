package com.novapixelnetwork.novapixelstaff.cmd;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.novapixelnetwork.novapixelstaff.NovaPixelStaff;
import com.novapixelnetwork.novapixelstaff.api.Colour;
import com.novapixelnetwork.novapixelstaff.api.Configuration;
import com.novapixelnetwork.novapixelstaff.api.PermissionsHook;

public class Staff implements CommandExecutor {
	
	NovaPixelStaff plugin;
	PermissionsHook pHook;
	
	public Staff(NovaPixelStaff plugin) {
		this.plugin = plugin;
		this.pHook = new PermissionsHook(plugin);
		
		plugin.getCommand("staff").setExecutor(this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(cmd.getLabel().equalsIgnoreCase("Staff")) {
				if(p.hasPermission("npstaff.staff")) {
				
				if(args.length == 0) {
					if(p.hasPermission("npstaff.staff.view")) {
				
				if(plugin.hasPermissionsEx) {
					Set<String> ranks = Configuration.getConfiguration().getConfigurationSection("Ranks").getKeys(false);
					  p.sendMessage("             *------------------------------------* ");                           
p.sendMessage(Colour.translateColor("             |                    &5Online &7Staff                     |", '&'));
					  p.sendMessage("             *------------------------------------* ");
					for(String rank : ranks) {
						StringBuilder sb = new StringBuilder();
						for(Player pl : Bukkit.getOnlinePlayers()) {
							if(pHook.getPex().getUser(pl.getName()).inGroup(rank)) {
								sb.append(pl.getName());
								sb.append(", ");
							}
						}
						if(sb.length() == 0) {
							rank = "";
							continue;
						}
						String rankName = Configuration.getConfiguration().getString("Ranks." + rank + ".displayColor") + rank;
						p.sendMessage(Colour.centerText(Colour.translateColor(rankName, '&')));
						p.sendMessage(Colour.centerText(sb.substring(0, sb.length() - 2)));
						p.sendMessage("");
					}
					
				} else {
					p.sendMessage(Colour.translateColor("No Permissions Plugin!", '&'));
					return false;
				}
					} else {
						sender.sendMessage(plugin.getPrefix() + Colour.translateColor(" &cYou cannot send this command!", '&'));
						return false;
					}
			  } else if(args.length == 1) {
				  if(args[0].equalsIgnoreCase("reload")) {
					if(p.hasPermission("npstaff.staff.reload")) {
					  try {
						  Configuration.reloadConfiguration();
						  p.sendMessage(plugin.getPrefix() + " The configuration has been reloaded.");
					  } catch (Exception e) {
						  e.printStackTrace();
						  plugin.getConsole().sendMessage(plugin.getPrefix() + Colour.translateColor("&cCould not reload configuration!", '&'));
					  }
				     } else {
						sender.sendMessage(plugin.getPrefix() + Colour.translateColor(" &cYou cannot send this command!", '&'));
						return false;				    	 
				     }
				  }
			  }
			} else {
				sender.sendMessage(plugin.getPrefix() + Colour.translateColor(" &cYou cannot send this command!", '&'));
				return false;
			}
		  }
			
		} else {
			sender.sendMessage(plugin.getPrefix() + Colour.translateColor(" &cYou cannot send this command!", '&'));
			return false;
		}
		
		return false;
	}

}
