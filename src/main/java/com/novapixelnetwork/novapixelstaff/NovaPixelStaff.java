package com.novapixelnetwork.novapixelstaff;

import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.novapixelnetwork.novapixelstaff.api.Colour;
import com.novapixelnetwork.novapixelstaff.api.Configuration;
import com.novapixelnetwork.novapixelstaff.cmd.Staff;


public class NovaPixelStaff extends JavaPlugin {
	
	ConsoleCommandSender console = getServer().getConsoleSender();
	String prefix = Colour.translateColor("&f[&5Nova&7Pixel&f]", '&');
	public boolean hasPermissionsEx;
	
	Configuration configuration;
	
	public void onEnable() {
		console.sendMessage(prefix + " NovaPixelStaff enabling with v" + getDescription().getVersion());
		
		configuration = new Configuration(this, "/", "staffList.yml");
		new Staff(this);
		
		hasPermissionsEx = false;
		if(checkPermissionsEx()) {
			hasPermissionsEx = true;
		}
		
		console.sendMessage(ChatColor.WHITE + "DEVELOPER!!!! REMOVE BEFORE RELEASE!");
		console.sendMessage("PermissionsEx: " + hasPermissionsEx);
	}
	
	public void onDisable() {
	}
	
	public ConsoleCommandSender getConsole() {
		return console;
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	private boolean checkPermissionsEx() {
		if(getServer().getPluginManager().getPlugin("PermissionsEx") != null) {
			return true;
		} else {
			return false;
		}
	}
	
}
