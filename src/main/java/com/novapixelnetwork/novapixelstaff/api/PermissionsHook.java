package com.novapixelnetwork.novapixelstaff.api;

import com.novapixelnetwork.novapixelstaff.NovaPixelStaff;

import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PermissionsHook {
	
	PermissionsEx pex;
	NovaPixelStaff plugin;
	
	public PermissionsHook(NovaPixelStaff plugin) {
		this.plugin = plugin;
	}
	
	public PermissionsEx getPex() {
		return pex;
	}
	
}
