package com.novapixelnetwork.novapixelstaff.api;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Colour {
	
	public static String translateColor(String text, char operator) {
		return ChatColor.translateAlternateColorCodes(operator, text);
	}
	
	
	public static String centerText(String text) {
	    int maxWidth = 80,
	            spaces = (int) Math.round((maxWidth-1.4*ChatColor.stripColor(text).length())/2);
	    return StringUtils.repeat(" ", spaces)+text;
	}
	
}
