package com.novapixelnetwork.novapixelstaff.api;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.novapixelnetwork.novapixelstaff.NovaPixelStaff;

public class Configuration {
	
	static File file;
	static FileConfiguration fileConfiguration;
	static String filePath, fileName;
	static NovaPixelStaff plugin;
	
	@SuppressWarnings("static-access")
	public Configuration(NovaPixelStaff plugin, String filePath, String fileName) {
		this.plugin = plugin;
		this.filePath = filePath;
		this.fileName = fileName;
		
		loadConfig(filePath, fileName);
		getConfiguration().createSection("Ranks");
		getConfiguration().set("Ranks.Admin.displayColor", "&c");
	}
	
	private static void loadConfig(String filePath, String fileName) {
		if(file == null) {
			try {
				file = new File(plugin.getDataFolder() + filePath + fileName);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		fileConfiguration = YamlConfiguration.loadConfiguration(file);
		saveConfiguration();
	}
	
	public static void saveConfiguration() {
		try {
			fileConfiguration.save(file);
		} catch (IOException e) {
			plugin.getConsole().sendMessage(plugin.getPrefix() + Colour.translateColor(" &cCould not save configuration file!", '&'));
			e.printStackTrace();
		}
	}
	
	public static FileConfiguration getConfiguration() {
		if(fileConfiguration == null) {
			loadConfig(filePath, fileName);
			plugin.getConsole().sendMessage(plugin.getPrefix() + Colour.translateColor(" &cCould not retrieve configuration file; Creating file!", '&'));
		}
		return fileConfiguration;
	}
	
	public static void reloadConfiguration() {
		loadConfig(filePath, fileName);
	}

}
	